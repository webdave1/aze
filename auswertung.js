//Stempeldaten aus DB holen,verarbeiten und in Kalender eintragen
function getphpName(JJJJ, MM)
{
    var zeit = new Array();
    var zeit_h = new Array();
    var zeit_m = new Array();
    var Beginn = new Array();
    var Stop = new Array();
    var azh = '';
    var azm = '';
    var Datum = '';
    var text = new Array();
    Zeit_error = '';

    req.open("GET", 'get_name.php?PersNr=' + PersNr + '&Jahr=' + JJJJ + '&Monat=' + MM + "&was=get&t=" + (new Date()).getTime(), false);
    req.send(null);

    data = req.responseText;

    line = data.split("|");

    //Daten aus SQL Tabelle verarbeiten
    for (var a = 0; a < line.length - 1; a++)
    {
        col = line[a].split("^");

        A_Beginn = (col[0].substr(0, 16)).split(" ");
        A_Ende = (col[1].substr(0, 16)).split(" ");

        //Check Zeiten!!!!
        //Tag
        Z = parseInt(A_Beginn[0].substr(A_Beginn[0].length - 2, 2), 10);
        document.getElementById(T_Cell[Z]).style.color = '#000000';
        if (Datum == A_Beginn[0])
        {

            checkEndZeit = Stop[Z].split(":");
            letzter_OUT_h = parseInt(checkEndZeit[0], 10);
            letzter_OUT_m = parseInt(checkEndZeit[1], 10);

            checkStartZeit = A_Beginn[1].split(":");
            NeuerStart_h = parseInt(checkStartZeit[0], 10);
            NeuerStart_m = parseInt(checkStartZeit[1], 10);

            //if( (letzter_OUT_h > NeuerStart_h) ||( (letzter_OUT_h == NeuerStart_h)&&(letzter_OUT_m < NeuerStart_m)))
            if ((letzter_OUT_h > NeuerStart_h) || ((letzter_OUT_h == NeuerStart_h) && (letzter_OUT_m > NeuerStart_m)))
            {
                document.getElementById(T_Cell[Z]).style.color = '#FF0000';
            }
        }


        //Datum
        Datum = A_Beginn[0];



        //Startzeit (Erste anfang an diesem Tag)
        if (Beginn[Z] == undefined)
            Beginn[Z] = A_Beginn[1];

        //Endzeit (Letztes Ende an diesem Tag)
        Stop[Z] = A_Ende[1];

        if (Stop[Z] == '23:59')
        {
            document.getElementById(T_Cell[Z]).style.color = '#FF0000';
        }



        //ArbeitsZeit Netto in Minuten
        if (zeit[Z] == undefined)
            zeit[Z] = parseInt(col[2], 10);
        else
            zeit[Z] = parseInt(zeit[Z], 10) + parseInt(col[2], 10);
    }

    for (var b = 1; b <= maxDay; b++)
    {
        var zh_s = '';
        var zm_s = '';
        if (zeit[b] != undefined)
        {
            //Arbeitszeit von Minuten im Echte Zeit h:mm
            zh_s = parseInt(zeit[b] / 60);
            zeit_f = parseFloat((zeit[b] / 60) - zh_s);

            zm_s = Math.round(parseFloat(zeit_f * 60));//Ominöse 1 minute Pause entsteht hier warum??

            if (zm_s > 59)
            {
                zm_s = zm_s - 60;
                zh_s = zh_s + 1;
            }

            // Minuten  2Stellig
            if (String(zm_s).length == 1)
                zm_s = '0' + zm_s;

            //Differenz aus anwesenheit und arbeitszeit => Pausenzeit
            B = Beginn[b].split(":");
            ST = Stop[b].split(":");
            P_H = (ST[0] - B[0]) - parseInt(zh_s, 10);
            P_M = (ST[1] - B[1]) - parseInt(zm_s, 10);
            if (String(P_M).substr(0, 1) == '-')
            {
                P_M = P_M + 60;
                P_H = P_H - 1;
                if (String(P_M).substr(0, 1) == '-')
                {
                    P_M = P_M + 60;
                    P_H = P_H - 1;
                }
            }

            // Minuten  2Stellig
            if (String(P_M).length == 1)
                P_M = '0' + P_M;

            //Prüfen on mindest Pausenzeit eingehalten
            // bis 6h Arbeitszeit echte Pausenzeit 
            if ((parseInt(zh_s, 10)) >= 6)
            {
                // ab 9h Arbeitszeit min 45 minuten Pause
                if ((parseInt(zh_s, 10)) >= 9)
                {
                    if (((parseInt(P_M, 10)) < 45) && ((parseInt(P_H, 10)) == 0))
                    {
                        PDIFF = 45 - P_M;
                        zm_s = zm_s - PDIFF;
                        if (String(zm_s).substr(0, 1) == '-')
                        {
                            zm_s = zm_s + 60;
                            zh_s = zh_s - 1;
                        }
                        if (String(zm_s).length == 1)
                            zm_s = '0' + zm_s;

                        P_M = 45;
                    }
                }
                else
                {
                    // ab 6h Arbeitszeit min 30 minuten Pause
                    if (((parseInt(P_M, 10)) < 30) && ((parseInt(P_H, 10)) == 0))
                    {
                        PDIFF = 30 - P_M;
                        zm_s = zm_s - PDIFF;
                        if (String(zm_s).substr(0, 1) == '-')
                        {
                            zm_s = zm_s + 60;
                            zh_s = zh_s - 1;
                        }
                        if (String(zm_s).length == 1)
                            zm_s = '0' + zm_s;
                        P_M = 30;
                    }
                }
            }
            //Summe Arbeitszeit Monat
            if (azh != '')
                azh = parseInt(azh, 10) + parseInt(zh_s, 10);
            else
                azh = parseInt(zh_s, 10);

            if (azm != '')
                azm = parseInt(azm, 10) + parseInt(zm_s, 10);
            else
                azm = parseInt(zm_s, 10);

            if (azm > 59)
            {
                azm = azm - 60;
                azh = azh + 1;
            }
            if (String(azm).length == 1)
                azm = '0' + azm;
            //In Tabelle schreiben		  
            table_string = 'Beginn:&nbsp;' + Beginn[b] + '<br>';
            table_string = table_string + 'Ende:&nbsp;' + Stop[b] + '<br>';
            table_string = table_string + 'Zeit:&nbsp;' + zh_s + ":" + zm_s + '<br>';
            table_string = table_string + 'Pause:&nbsp;' + P_H + ":" + P_M + '<br>';
            PDIFF = 0;
            document.getElementById(T_Cell[b]).innerHTML = document.getElementById(T_Cell[b]).innerHTML + table_string;
            if (document.getElementById(T_Cell[b]).style.color == 'rgb(255, 0, 0)')
                Zeit_error += 'ERRORPersNr=' + PersNr + '&Jahr=' + JJJJ + '&Monat=' + MM + '&Tag=' + b + '&was=korrektur&t=' + (new Date()).getTime();
            //Zeit_error += 'ERROR'+b+'|'+b+'.'+MM+'.'+JJJJ+'|'+Beginn[b]+'|'+ Stop[b];   

        }
    }
    //DIFFERENZ berechnen
    n_Min = (60 - azm);
    n_Stu = ((azh - soll));
    if (String(n_Stu).substr(0, 1) != '-')
        n_Stu = n_Stu + 1;

    if (n_Stu == 0 - soll + 1)
        n_Stu = soll;

    if ((n_Stu == 0) && (n_Min != '00') && (n_Min > 0))
        n_Stu = '-' + n_Stu;

    if (n_Stu > 0)
    {
        n_Min = (60 - n_Min);
        n_Stu = (azh - soll);
    }

    if (String(n_Min).length == 1)
        n_Min = '0' + n_Min;
    if (n_Min == 60)
        n_Min = '00';

    if (n_Min != '00')
        n_Stu = n_Stu + 1;

    if ((String(n_Stu).substr(0, 1) != '-') && (n_Stu != 0))
        n_Stu = n_Stu - 1;

    document.getElementById('info5').innerHTML = document.getElementById('info5').innerHTML + 'IST: ' + azh + ':' + azm + '<br>DIFF: ' + n_Stu + ":" + n_Min;
    if (MM == (H_Jahresmonat + 1))
        letztenMonat()

    console.log('ERROR: ' + Zeit_error);
}


function letztenMonat()
{
    req.open("GET", 'get_name.php?PersNr=' + PersNr + '&was=ue&t=' + (new Date()).getTime(), false);
    req.send(null);
    UH = (req.responseText).split(".");
    noch = UH[0].split(":");
    k_update = UH[1];
    document.getElementById('info5').innerHTML = document.getElementById('info5').innerHTML + '<br><u>Konto: ' + UH[0] + '</u>';
    if (String(noch[0]).substr(0, 1) != '-')
        noch[1] = '-' + noch[1];

    UEh_neu = parseInt(noch[0], 10) + parseInt(n_Stu, 10);
    UEm_neu = parseInt(noch[1], 10) + parseInt(n_Min, 10);

    if (String(UEm_neu).substr(0, 1) == '-')
        UEm_neu = String(UEm_neu).substr(1, String(UEm_neu).length);

    if (UEm_neu > 59)
    {
        UEm_neu = UEm_neu - 60;
        UEh_neu = UEh_neu + 1;
    }
    if (String(UEm_neu).length == 1)
        UEm_neu = '0' + UEm_neu;

    document.getElementById('info5').innerHTML = document.getElementById('info5').innerHTML + '<br>Rest: ' + UEh_neu + ':' + UEm_neu;
}