<?php

header('Content-type: text/html; charset=UTF-8');
error_reporting(E_STRICT);
date_default_timezone_set("Europe/Berlin");
$PersNr = '';
$PersNr = $_GET["PersNr"];
$was = '';
$was = $_GET["was"];
define('DB_USER', 'test');
define('DB_PASS', 'test');
define('DB_DATABASE', 'testDB');
$connection = odbc_connect(DB_DATABASE, DB_USER, DB_PASS);

if (!$connection) {
    echo "Keine Verbindung moeglich!\n";
    exit;
}

//Die stempelvorgaenge aus de Monat auslesen
if ($was == 'get') {
    $Jahr = '';
    $Jahr = $_GET["Jahr"];
    $Monat = '';
    $Monat = $_GET["Monat"];
    $sqlQuery = " SELECT tblAzInOut.Beginn , 
                         tblAzInOut.Ende , 
                         DATEDIFF(minute, tblAzInOut.Beginn,tblAzInOut.Ende) ,
                         tblPerson.PersID
	         FROM tblPerson inner JOIN tblAzInOut ON tblPerson.PersID = tblAzInOut.UserID 
	         where tblPerson.PersNr like '$PersNr'
                       AND datepart(year, tblAzInOut.Beginn) = '$Jahr'  
                       AND datepart(month, tblAzInOut.Beginn) = '$Monat' 
                       AND  tblAzInOut.Ende IS NOT NULL 
                 order by tblAzInOut.Beginn"; // Status != 'in
    $erg = odbc_exec($connection, $sqlQuery);

    while ($row = odbc_fetch_object($erg)) {
        foreach ($row as $item) {
            echo $item;
            echo "^";
        }
        echo "|";
    }
}
if ($was == 'set') {
    $sqlQuery = " SELECT tblPerson.PersID
	          FROM tblPerson inner JOIN tblAzInOut ON tblPerson.PersID = tblAzInOut.UserID 
	          where tblPerson.PersNr like '$PersNr'";
    $erg = odbc_exec($connection, $sqlQuery);

    while ($row = odbc_fetch_object($erg)) {
        foreach ($row as $item) {
            $PersID = $item;
        }
    }

    $Start = '';
    $Start = $_GET["Start"];
    $End = '';
    $End = $_GET["End"];
    $T_Start = date("Y-m-d H:i:s", strtotime("$Start"));
    $T_End = date("Y-m-d H:i:s", strtotime("$End"));
    $neu = "INSERT INTO tblAzInOut ( UserID, Beginn, Ende, Status)VALUES(  '$PersID', '$T_Start', '$T_End', 'extOUT')";
    $neu = odbc_exec($connection, $neu) or die(odbc_errormsg($connection));

    if ($neu == true)
        echo "1";
    else
        echo "0";
}

if ($was == 'ue') {

    $sqlQuery = " SELECT tblStundenkonto.Stunden
	          FROM tblPerson inner JOIN tblStundenkonto ON tblPerson.PersID = tblStundenkonto.PersID 
	          where tblPerson.PersNr like '$PersNr'";
    $erg = odbc_exec($connection, $sqlQuery) or die(odbc_errormsg($connection));

    while ($row = odbc_fetch_object($erg)) {
        foreach ($row as $item) {
            echo $item;
        }
    }
}
if ($was == 'heute') {
    $Jahr = '';
    $Jahr = $_GET["Jahr"];
    $Monat = '';
    $Monat = $_GET["Monat"];
    $Tag = '';
    $Tag = $_GET["Tag"];
    echo "<table style='border-style:solid; border-width:thin'>";
    echo '<tr bgcolor="#d9d9d9"  >';
    echo "<th colspan='2'>" . $Tag . "." . $Monat . "." . $Jahr . "</th>";
    echo "</tr>";
    echo "<tr bgcolor='#77C5E6'>";
    echo "<th style='height:25px; width:50px;' >IN</th>";
    echo " <th style='height:25px; width:50px;' >OUT</th>";
    echo "</tr>";
    echo "<tr>";
    $sqlQuery = " SELECT tblAzInOut.Beginn , 
                         tblAzInOut.Ende 
	          FROM tblPerson inner JOIN tblAzInOut ON tblPerson.PersID = tblAzInOut.UserID 
	          where tblPerson.PersNr like '$PersNr' 
                        AND datepart(year, tblAzInOut.Beginn) = '$Jahr'  
                        AND datepart(month, tblAzInOut.Beginn) = '$Monat' 
                        AND datepart(day, tblAzInOut.Beginn) = '$Tag'  
                  order by tblAzInOut.Beginn";
    $erg = odbc_exec($connection, $sqlQuery);

    while ($row = odbc_fetch_object($erg)) {
        foreach ($row as $item) {
            echo "<td>" . date("H:i", strtotime("$item")) . "</td>";
        }
        echo "</tr>";
    }
    echo "</table>";
}

if ($was == 'korrektur') {
    $Jahr = '';
    $Jahr = $_GET["Jahr"];
    $Monat = '';
    $Monat = $_GET["Monat"];
    $Tag = '';
    $Tag = $_GET["Tag"];
    $sqlQuery = " SELECT tblAzInOut.Beginn , 
                         tblAzInOut.Ende 
	          FROM tblPerson inner JOIN tblAzInOut ON tblPerson.PersID = tblAzInOut.UserID 
	          where tblPerson.PersNr like '$PersNr' 
                        AND datepart(year, tblAzInOut.Beginn) = '$Jahr'  
                        AND datepart(month, tblAzInOut.Beginn) = '$Monat' 
                        AND datepart(day, tblAzInOut.Beginn) = '$Tag'  
                  order by tblAzInOut.Beginn";
    $erg = odbc_exec($connection, $sqlQuery);

    echo $Tag . "." . $Monat . "." . $Jahr . '<BR>';
    while ($row = odbc_fetch_object($erg)) {
        foreach ($row as $item) {
            echo date("H:i", strtotime("$item")) . "|";
        }
        echo '<BR>';
    }
}


if ($was == 'konto') {
    $Stunden = $_GET["Stunden"];
    $sqlQuery = " UPDATE  tblStundenkonto Set Stunden = '$Stunden' WHERE PersID = '$PersNr'";
    $erg = odbc_exec($connection, $sqlQuery) or die(odbc_errormsg($connection));

    while ($row = odbc_fetch_object($erg)) {
        foreach ($row as $ID) {
            echo $ID . "!";
        }
    }
}
?>  