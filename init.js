window.onload = init;
var Versin = '2.0';
req = new XMLHttpRequest();
var version = '2.1';
var H_derTag = '';
var H_Jahr = '';
var H_Jahresmonat = '';
var T_Cell = new Array();
var maxDay = '';
var Username = '';
var A_MM = '';
var A_JJJJ = '';
var wo = '';
var date = '';
var stunden_letzten_Monat = 0;
var n_Stu = '';
var n_Min = '';
var SollStd = 0;
var T_TDW;
var k_update;
var PersID;
var konto_frei = 1;
var Zeit_error = '';
var PersName;
var KorrZeiten;

function init()
{
    info = (window.location.search.substring(1)).split("|");
    Username = 'Max';//unescape(info[0]);
    PersNr = '0815';//info[1];
    PersName = 'Max, Mustermann';//unescape(info[2]);
    SollStd = '8';//info[3];
    PersID = '23';//info[4];

    document.getElementById('m').innerHTML = 'Hallo ' + PersName + '<br>Keine Daten gefunden';

    show_time();

    //Kalender fuellen
    cal_date((H_Jahresmonat + 1), H_Jahr);
}

Array.prototype.search = function(value, strict)
{
    if (typeof value == "undefined")
        return false;

    var retVal = false;
    if (strict)
    {
        for (key in this)
        {
            if (this[key] === value)
            {
                retVal = key;
                break;
            }
        }
    }
    else
    {
        for (key in this)
        {
            if (this[key] == value)
            {
                retVal = key;
                break;
            }
        }
    }
    return retVal;
}

//Eingabemaske fuer neuen eintrag verstecken
function cancel_getdateof()
{
    wo = '';
    document.getElementById('Start').value = '';
    document.getElementById('End').value = '';
    document.getElementById('datum').innerHTML = '';
    document.getElementById('edit').style.visibility = "hidden";
}

//Neuen Eintrag speichern
function new_date()
{
    if (((String(document.getElementById('Start').value).length) >= 4) && ((String(document.getElementById('End').value).length) >= 4))
    {
        StartTime = date + " " + document.getElementById('Start').value;
        EndTime = date + " " + document.getElementById('End').value;
        cancel_getdateof();
        req.open("GET", 'get_name.php?PersNr=' + PersNr + '&Start=' + StartTime + '&End=' + EndTime + "&was=set&t=" + (new Date()).getTime(), false);
        req.send(null);
        data = req.responseText;
        if (req.responseText == 1)
            cal_date(A_MM, A_JJJJ);
        else
            alert("UUUPS Fehler!");
    }
    else
        alert("Zeit Bitte Richtig eingeben z.B.: 8:00");
}

//Einträge an diesem Tag abfragen
function WorkToday(e, td)
{
    clearTimeout(T_TDW);
    x = e.pageX || window.event.x;
    y = e.pageY || window.event.y;
    D_wo = T_Cell.search(td);
    if (D_wo)
    {
        req.open("GET", 'get_name.php?PersNr=' + PersNr + '&Jahr=' + A_JJJJ + '&Monat=' + A_MM + '&Tag=' + D_wo + "&was=heute&t=" + (new Date()).getTime(), false);
        req.send(null);
        dataToday = req.responseText;

        top.document.getElementById('Stempelkarte').innerHTML = dataToday;
        T_TDW = setTimeout("top.document.getElementById('Stempelkarte').innerHTML = ''", 5000);
    }
}


function getdateof(id)
{
    if (wo == '')
    {
        wo = T_Cell.search(id);
        date = A_JJJJ + "-" + A_MM + "-" + wo;//A_MM+"."+wo+"."+A_JJJJ;//
        document.getElementById('datum').innerHTML = wo + "." + A_MM + "." + A_JJJJ;
        document.getElementById('edit').style.visibility = "visible";
    }
}

function wechsel_M(wohin)
{
    if (wo == '')
    {
        N_MM = parseInt(wohin, 10) + parseInt(A_MM, 10);
        if (N_MM < 1)
            N_MM = 12;
        if (N_MM > 12)
            N_MM = 1;
        cal_date(N_MM, A_JJJJ);
    }
}

function wechsel_J(wohin)
{
    if (wo == '')
    {
        N_JJJJ = parseInt(wohin, 10) + parseInt(A_JJJJ, 10);
        cal_date(A_MM, N_JJJJ);
    }
}


//Darstellung des Kalenders
function cal_date(MM, JJJJ)
{
    T_Cell = new Array();
    A_MM = MM;
    A_JJJJ = JJJJ;
    W_MM = MM;
    if (W_MM < 0)
        W_MM = 11;
    if (W_MM > 11)
        W_MM = 0;
    Z_MM = MM - 2;
    if (Z_MM < 0)
        Z_MM = 11;
    if (Z_MM > 11)
        Z_MM = 0;
    V_MM = MM - 1;
    if (V_MM < 0)
        V_MM = 11;
    if (V_MM > 11)
        V_MM = 0;

    var Monat = new Array("Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember");
    document.getElementById('m').innerHTML = PersName + '<br>' + Monat[V_MM] + "  " + JJJJ;
    document.getElementById('But1').value = (JJJJ - 1);
    document.getElementById('But2').value = (JJJJ + 1);
    document.getElementById('But3').value = Monat[Z_MM];
    document.getElementById('But4').value = Monat[W_MM];

    for (var rid = 1; rid <= 6; rid++)
    {
        for (var cid = 0; cid <= 6; cid++)
        {
            document.getElementById(rid + '_' + cid).innerHTML = '';
            document.getElementById(rid + '_' + cid).style.color = '#000000';
            document.getElementById(rid + '_' + cid).style.backgroundColor = "#FFFFFF";
            document.getElementById(rid + '_' + cid).style.height = "2px";
        }
    }
    document.getElementById('KW_1').innerHTML = '';
    document.getElementById('KW_5').innerHTML = '';
    document.getElementById('KW_6').innerHTML = '';
    document.getElementById('info5').innerHTML = '';

    var T_row = 1;

    //Anzahl der tage im Monat und evtl Feiertage
    var text = new Array();

    req.open("GET", 'get_days.php?Jahr=' + JJJJ + '&Monat=' + MM + "&t=" + (new Date()).getTime(), false);
    req.send(null);
    M_data = req.responseText;
    M_info = M_data.split("|");
    maxDay = M_info[0];
    WD = maxDay;

    for (var d = 1; d <= maxDay; d++)
    {
        // Eingabe-Datum in mSek umwandeln
        var timeObj = new Date();
        var time = Date.UTC(JJJJ, (MM - 1), d);

        //KW ermitteln NICHT von mir, irgendwo im www gefunden.
        Datum = new Date(JJJJ, (MM - 1), d);

        DoDat = donnerstag(Datum);
        kwjahr = DoDat.getFullYear();
        DoKW1 = donnerstag(new Date(kwjahr, 0, 4));
        kw = Math.floor(1.5 + (DoDat.getTime() - DoKW1.getTime()) / 86400000 / 7);
        // Wochentag auslesen (0-6)
        timeObj.setTime(time);
        weekday = timeObj.getDay();
        if ((weekday == 1) && (d != 1))
            T_row++;

        T_Week = T_row + "_" + weekday;
        var week = "SoMoDiMiDoFrSa";
        T_Cell[d] = T_Week;
        T_KW = 'KW_' + T_row;
        document.getElementById(T_KW).innerHTML = '<br>' + kw + '<br>' + '<br>';
        document.getElementById(T_Week).innerHTML = d + "." + MM + "." + JJJJ + '<br><br>';
        document.getElementById(T_KW).style.height = "85px";
        document.getElementById(T_Week).style.height = "110px";

        if ((weekday == 0) || (weekday == 6))
        {
            document.getElementById(T_Week).style.backgroundColor = "#AEAEAE";
            WD = WD - 1;
        }
        else
            document.getElementById(T_Week).style.backgroundColor = "#d9d9d9";
        if ((d == H_derTag) && ((MM - 1) == H_Jahresmonat) && (JJJJ == H_Jahr))
            document.getElementById(T_Week).style.backgroundColor = "#9EEE9E";
    }

    //Feiertage
    if (M_info[1] - 1 != -1)
    {
        F_days = M_info[2].split("&");
        for (var f = 0; f <= M_info[1] - 1; f++)
        {
            FD_Name = F_days[f].split("^");
            FD = parseInt(FD_Name[1], 10);
            if ((FD_Name[2] != 0) && (FD_Name[2] != 6))
                WD = WD - 1;
            document.getElementById(T_Cell[FD]).innerHTML = document.getElementById(T_Cell[FD]).innerHTML + FD_Name[0] + '<br>';
        }
    }
    soll = parseInt(WD, 10) * SollStd;
    soll_e = (String(soll).split("."));
    soll = soll_e[0];
    soll_mi = '0.' + soll_e[1];
    sollM = parseFloat(soll_mi, 10) * 60;
    if (String(sollM).length == 1)
        sollM = '0' + sollM;

    document.getElementById('info5').innerHTML = 'Stunden<br>SOLL: ' + soll + ":" + Math.round(sollM) + "<br>";
    getphpName(JJJJ, MM);
}

//KW ermitteln NICHT von mir, irgendwo im www gefunden.
function donnerstag(datum)
{
    var Do = new Date();
    Do.setTime(datum.getTime() + (3 - ((datum.getDay() + 6) % 7)) * 86400000);
    return Do;
}