<?php

header('Content-type: text/html; charset=ISO-8859-1');
$Jahr = '';
$Jahr = $_GET["Jahr"];
$Monat = '';
$Monat = $_GET["Monat"];

//Anzahl der Feiertage im Monat
$eventcount = 0;

//Feiertage: Name^Datum
$eventname = '|';

error_reporting(E_STRICT);
date_default_timezone_set("Europe/Berlin");

//Tage im Monat
echo date("t", mktime(0, 0, 0, $Monat, date("d^w"), $Jahr)) . '|';

//Ostern ermitteln zum berechnen der Feiertage
$d = (((255 - 11 * ($Jahr % 19)) - 21) % 30) + 21;
$delta = $d + ($d > 48) + 6 - (($Jahr + $Jahr / 4 + $d + ($d > 48) + 1) % 7);
$easter = strtotime("+$delta days", mktime(0, 0, 0, 3, 1, $Jahr));

if (date("m", $easter) == $Monat) {
    $eventname .= 'Ostern^' . date("d^w", $easter) . '&';
    $eventcount ++;
}
if (date("m", mktime(0, 0, 0, 1, 1, $Jahr)) == $Monat) {
    $eventname .= 'Neujahr^' . date("d^w", mktime(0, 0, 0, 1, 1, $Jahr)) . '&';
    $eventcount ++;
}
/* if(date("m",strtotime("-48 days", $easter))== $Monat){
  $eventname .= 'Rosenmontag^'.date("d^w",strtotime("-48 days", $easter)).'&';
  $eventcount ++;
  } */
if (date("m", mktime(0, 0, 0, 5, 1, $Jahr)) == $Monat) {
    $eventname .= 'Tag der Arbeit^' . date("d^w", mktime(0, 0, 0, 5, 1, $Jahr)) . '&';
    $eventcount ++;
}
if (date("m", strtotime("-2 days", $easter)) == $Monat) {
    $eventname .= 'Karfreitag^' . date("d^w", strtotime("-2 days", $easter)) . '&';
    $eventcount ++;
}
if (date("m", strtotime("+1 day", $easter)) == $Monat) {
    $eventname .= 'Ostermontag^' . date("d^w", strtotime("+1 day", $easter)) . '&';
    $eventcount ++;
}
if (date("m", strtotime("+39 days", $easter)) == $Monat) {
    $eventname .= 'Himmelfahrt^' . date("d^w", strtotime("+39 days", $easter)) . '&';
    $eventcount ++;
}
if (date("m", strtotime("+49 days", $easter)) == $Monat) {
    $eventname .= 'Pfingsten^' . date("d^w", strtotime("+49 days", $easter)) . '&';
    $eventcount ++;
}
if (date("m", strtotime("+50 days", $easter)) == $Monat) {
    $eventname .= 'Pfingstmontag^' . date("d^w", strtotime("+50 days", $easter)) . '&';
    $eventcount ++;
}
/* if(date("m",strtotime("+60 days", $easter))== $Monat){
  $eventname .= 'Fronleichnam^'.date("d^w",strtotime("+60 days", $easter)).'&';
  $eventcount ++;
  } */
if (date("m", mktime(0, 0, 0, 10, 3, $Jahr)) == $Monat) {
    $eventname .= 'Tag der dt Einheit^' . date("d^w", mktime(0, 0, 0, 10, 3, $Jahr)) . '&';
    $eventcount ++;
}
if (date("m", mktime(0, 0, 0, 12, 24, $Jahr)) == $Monat) {
    $eventname .= 'Heiligabend^' . date("d^w", mktime(0, 0, 0, 12, 24, $Jahr)) . '&';
    $eventcount ++;
}
if (date("m", mktime(0, 0, 0, 12, 25, $Jahr)) == $Monat) {
    $eventname .= '1. Weihnachts<br>feiertag^' . date("d^w", mktime(0, 0, 0, 12, 25, $Jahr)) . '&';
    $eventcount ++;
}
if (date("m", mktime(0, 0, 0, 12, 26, $Jahr)) == $Monat) {
    $eventname .= '2. Weihnachts<br>feiertag^' . date("d^w", mktime(0, 0, 0, 12, 26, $Jahr)) . '&';
    $eventcount ++;
}
if (date("m", mktime(0, 0, 0, 12, 31, $Jahr)) == $Monat) {
    $eventname .= 'Sylvester^' . date("d^w", mktime(0, 0, 0, 12, 31, $Jahr)) . '&';
    $eventcount ++;
}
/* if(date("m",strtotime("-11 days", strtotime("1 sunday", mktime(0,0,0,11,26,$Jahr))))== $Monat){
  $eventname .= 'Bussbettag^'.date("d^w",strtotime("-11 days", strtotime("1 sunday", mktime(0,0,0,11,26,$Jahr)))).'&';
  $eventcount ++;
  } */
if (date("m", strtotime("1 sunday", mktime(0, 0, 0, 11, 26, $Jahr))) == $Monat) {
    $eventname .= '1. Advent^' . date("d^w", strtotime("1 sunday", mktime(0, 0, 0, 11, 26, $Jahr))) . '&';
    $eventcount ++;
}
if (date("m", strtotime("2 sunday", mktime(0, 0, 0, 11, 26, $Jahr))) == $Monat) {
    $eventname .= '2. Advent^' . date("d^w", strtotime("2 sunday", mktime(0, 0, 0, 11, 26, $Jahr))) . '&';
    $eventcount ++;
}
if (date("m", strtotime("3 sunday", mktime(0, 0, 0, 11, 26, $Jahr))) == $Monat) {
    $eventname .= '3. Advent^' . date("d^w", strtotime("3 sunday", mktime(0, 0, 0, 11, 26, $Jahr))) . '&';
    $eventcount ++;
}
if (date("m", strtotime("4 sunday", mktime(0, 0, 0, 11, 26, $Jahr))) == $Monat) {
    $eventname .= '4. Advent^' . date("d^w", strtotime("4 sunday", mktime(0, 0, 0, 11, 26, $Jahr))) . '&';
    $eventcount ++;
}
echo $eventcount;
echo $eventname;
?>