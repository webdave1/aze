//Konto start
function konto()
{
    if (wo == '')
    {
        K_N_MM = parseInt(-1, 10) + parseInt(A_MM, 10);

        if (K_N_MM < 1)
            K_N_MM = 12;
        if (K_N_MM > 12)
            K_N_MM = 1;

        if (((k_update == undefined) || (k_update == K_N_MM)) && (K_N_MM == H_Jahresmonat) && (konto_frei == 1))
        {
            //if(Zeit_error == '') 
            cal_konto(K_N_MM, A_JJJJ);
            //else korrekturVorschlag();
        }
        else if (Zeit_error != '')
            korrekturVorschlag();//debuging!!!

    }
}


//Konto aktuallisieren Arbeitstage
function cal_konto(K_MM, K_JJJJ)
{
    konto_frei = 0;
    T_Cell = new Array();
    A_MM = K_MM;
    A_JJJJ = K_JJJJ;
    W_MM = K_MM;
    if (W_MM < 0)
        W_MM = 11;

    if (W_MM > 11)
        W_MM = 0;

    Z_MM = K_MM - 2;
    if (Z_MM < 0)
        Z_MM = 11;

    if (Z_MM > 11)
        Z_MM = 0;

    V_MM = K_MM - 1;

    if (V_MM < 0)
        V_MM = 11;
    if (V_MM > 11)
        V_MM = 0;

    var Monat = new Array("Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember");

    var T_row = 1;
    //Anzahl der tage im Monat und evtl Feiertage
    var text = new Array();
    req.open("GET", 'get_days.php?Jahr=' + K_JJJJ + '&Monat=' + K_MM + "&t=" + (new Date()).getTime(), false);
    req.send(null);
    M_data = req.responseText;
    M_info = M_data.split("|");
    maxDay = M_info[0];
    WD = maxDay;

    for (var d = 1; d <= maxDay; d++)
    {
        // Eingabe-Datum in mSek umwandeln
        var timeObj = new Date();
        var time = Date.UTC(K_JJJJ, (K_MM - 1), d);

        //KW ermitteln NICHT von mir, irgendwo im www gefunden.
        Datum = new Date(K_JJJJ, (K_MM - 1), d);

        DoDat = donnerstag(Datum);
        kwjahr = DoDat.getFullYear();
        DoKW1 = donnerstag(new Date(kwjahr, 0, 4));
        kw = Math.floor(1.5 + (DoDat.getTime() - DoKW1.getTime()) / 86400000 / 7);
        // Wochentag auslesen (0-6)
        timeObj.setTime(time);
        weekday = timeObj.getDay();
        if ((weekday == 1) && (d != 1))
            T_row++;
        T_Week = T_row + "_" + weekday;
        var week = "SoMoDiMiDoFrSa";
        T_Cell[d] = T_Week;
        T_KW = 'KW_' + T_row;
        if ((weekday == 0) || (weekday == 6))
            WD = WD - 1;
    }

    //Feiertage
    if (M_info[1] - 1 != -1)
    {
        F_days = M_info[2].split("&");
        for (var f = 0; f <= M_info[1] - 1; f++)
        {
            FD_Name = F_days[f].split("^");
            FD = parseInt(FD_Name[1], 10);
            if ((FD_Name[2] != 0) && (FD_Name[2] != 6))
                WD = WD - 1;
        }
    }
    soll = parseInt(WD, 10) * SollStd;
    soll_e = (String(soll).split("."));
    soll = soll_e[0];
    soll_mi = '0.' + soll_e[1];
    sollM = parseFloat(soll_mi, 10) * 60;
    if (String(sollM).length == 1)
        sollM = '0' + sollM;
    kontogetphpName(K_JJJJ, K_MM);
}


//Konto aktuallisieren Stunden
function kontogetphpName(K_JJJJ, K_MM)
{
    var zeit = new Array();
    var zeit_h = new Array();
    var zeit_m = new Array();
    var Beginn = new Array();
    var Stop = new Array();
    var azh = '';
    var azm = '';
    var text = new Array();
    req.open("GET", 'get_name.php?PersNr=' + PersNr + '&Jahr=' + K_JJJJ + '&Monat=' + K_MM + "&was=get&t=" + (new Date()).getTime(), false);
    req.send(null);
    data = req.responseText;
    line = data.split("|");

    for (var a = 0; a < line.length - 1; a++)
    {
        col = line[a].split("^");
        A_Beginn = (col[0].substr(0, 16)).split(" ");
        A_Ende = (col[1].substr(0, 16)).split(" ");

        //Datum
        Datum = A_Beginn[0];

        //Tag
        Z = parseInt(Datum.substr(Datum.length - 2, 2), 10);

        //Startzeit (Erste anfang an diesem Tag)
        if (Beginn[Z] == undefined)
            Beginn[Z] = A_Beginn[1];

        Stop[Z] = A_Ende[1];

        //ArbeitsZeit Netto in Minuten
        if (zeit[Z] == undefined)
            zeit[Z] = parseInt(col[2], 10);
        else
            zeit[Z] = parseInt(zeit[Z], 10) + parseInt(col[2], 10);

    }
    for (var b = 1; b <= maxDay; b++)
    {
        var zh_s = '';
        var zm_s = '';
        if (zeit[b] != undefined)
        {
            //Arbeitszeit von Minuten im Echte Zeit h:mm
            zh_s = parseInt(zeit[b] / 60);
            zeit_f = parseFloat((zeit[b] / 60) - zh_s);
            zm_s = Math.round(parseFloat(zeit_f * 60));
            if (zm_s > 59)
            {
                zm_s = zm_s - 60;
                zh_s = zh_s + 1;
            }

            // Minuten  2Stellig
            if (String(zm_s).length == 1)
                zm_s = '0' + zm_s;

            //Differenz aus anwesenheit und arbeitszeit => Pausenzeit
            B = Beginn[b].split(":");
            ST = Stop[b].split(":");
            P_H = (ST[0] - B[0]) - parseInt(zh_s, 10);
            P_M = (ST[1] - B[1]) - parseInt(zm_s, 10);

            if (String(P_M).substr(0, 1) == '-')
            {
                P_M = P_M + 60;
                P_H = P_H - 1;

                if (String(P_M).substr(0, 1) == '-')
                {
                    P_M = P_M + 60;
                    P_H = P_H - 1;
                }
            }
            // Minuten  2Stellig
            if (String(P_M).length == 1)
                P_M = '0' + P_M;
            //Prüfen on mindest Pausenzeit eingehalten
            // bis 6h Arbeitszeit echte Pausenzeit
            if ((parseInt(zh_s, 10)) >= 6)
            {
                // ab 9h Arbeitszeit min 45 minuten Pause
                if ((parseInt(zh_s, 10)) >= 9)
                {
                    if (((parseInt(P_M, 10)) < 45) && ((parseInt(P_H, 10)) == 0))
                    {
                        PDIFF = 45 - P_M;
                        zm_s = zm_s - PDIFF;
                        if (String(zm_s).substr(0, 1) == '-')
                        {
                            zm_s = zm_s + 60;
                            zh_s = zh_s - 1;
                        }
                        if (String(zm_s).length == 1)
                            zm_s = '0' + zm_s;
                        P_M = 45;
                    }
                }
                else
                {
                    // ab 6h Arbeitszeit min 30 minuten Pause
                    if (((parseInt(P_M, 10)) < 30) && ((parseInt(P_H, 10)) == 0))
                    {
                        PDIFF = 30 - P_M;
                        zm_s = zm_s - PDIFF;
                        if (String(zm_s).substr(0, 1) == '-')
                        {
                            zm_s = zm_s + 60;
                            zh_s = zh_s - 1;
                        }
                        if (String(zm_s).length == 1)
                            zm_s = '0' + zm_s;
                        P_M = 30;
                    }
                }
            }
            //Summe Arbeitszeit Monat
            if (azh != '')
                azh = parseInt(azh, 10) + parseInt(zh_s, 10);
            else
                azh = parseInt(zh_s, 10);

            if (azm != '')
                azm = parseInt(azm, 10) + parseInt(zm_s, 10);
            else
                azm = parseInt(zm_s, 10);
            if (azm > 59)
            {
                azm = azm - 60;
                azh = azh + 1;
            }
            if (String(azm).length == 1)
                azm = '0' + azm;

            PDIFF = 0;
        }
    }
    //DIFFERENZ berechnen
    n_Min = (60 - azm);
    n_Stu = ((azh - soll));
    if (String(n_Stu).substr(0, 1) != '-')
        n_Stu = n_Stu + 1;

    if (n_Stu == 0 - soll + 1)
        n_Stu = soll;

    if ((n_Stu == 0) && (n_Min != '00') && (n_Min > 0))
        n_Stu = '-' + n_Stu;

    if (n_Stu > 0)
    {
        n_Min = (60 - n_Min);
        n_Stu = (azh - soll);
    }
    if (String(n_Min).length == 1)
        n_Min = '0' + n_Min;

    if (n_Min == 60)
        n_Min = '00';

    if (n_Min != '00')
        n_Stu = n_Stu + 1;

    if ((String(n_Stu).substr(0, 1) != '-') && (n_Stu != 0))
        n_Stu = n_Stu - 1;


    req.open("GET", 'get_name.php?PersNr=' + PersNr + '&was=ue&t=' + (new Date()).getTime(), false);
    req.send(null);
    UH = req.responseText.split(".");
    noch = UH[0].split(":");
    if (String(noch[0]).substr(0, 1) == '-')
        noch[1] = '-' + noch[1];

    //Zeiten in Minuten
    uemin = ((parseInt(noch[0], 10) * 60) + parseInt(noch[1], 10));
    remin = ((parseInt(n_Stu, 10) * 60) + parseInt(n_Min, 10));
    komin = uemin + remin;
    UEh_neu = parseInt(komin / 60, 10);
    UEm_neu = komin - (UEh_neu * 60);

    if (String(UEm_neu).length == 1)
        UEm_neu = '0' + UEm_neu;

    if (parseInt(UEh_neu, 10) >= 20)
    {
        UEh_neu = '20';
        UEm_neu = '00';
    }
    req.open("GET", 'get_name.php?PersNr=' + PersID + '&was=konto&Stunden=' + UEh_neu + ':' + UEm_neu + '.' + (K_N_MM + 1) + '&t=' + (new Date()).getTime(), false);
    req.send(null);
    top.document.getElementById('kalender').src = './auswertung.html?' + Username + "|" + PersNr + "|" + PersName + "|" + SollStd + "|" + PersID;

}