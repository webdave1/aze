<?php
//Daten holen
$name = '';
$name =  $_GET["name"];
//Splitten
$data = '';
$data =  explode("<BR>",$_GET["data"]);
$mail_content = '';
//Untereinander schreiben
for( $i = 0; $i <=(count($data)-1); $i++ )
$mail_content .= $data[$i]."\n";

//Klasse einbinden
require('phpmailer/class.phpmailer.php');

//Instanz von PHPMailer bilden
$mail = new PHPMailer();

//Absenderadresse der Email setzen
$mail->From = "noreply@aze.de";

//Name des Abenders setzen
$mail->FromName = $name;

//Empfängeradresse setzen
$mail->AddAddress("david.muellerchen@avionic-design.de");

//Betreff der Email setzen
$mail->Subject = "AZE Korrektur";
//Text der EMail setzen
$mail->Body = $mail_content;

//EMail senden und überprüfen ob sie versandt wurde
if(!$mail->Send())
{
//$mail->Send() liefert FALSE zurück: Es ist ein Fehler aufgetreten
echo "Die Email konnte nicht gesendet werden";
echo "Fehler: " . $mail->ErrorInfo;
}
else
{
//$mail->Send() liefert TRUE zurück: Die Email ist unterwegs
echo "Die Email wurde versandt.";
}
?>